import os
import sys
from tkinter import *
from tkinter import Tk, filedialog, messagebox
from tkinter.messagebox import showerror, showinfo, showwarning
from tkinter.filedialog import askopenfilename, askopenfilenames

import subprocess, shlex

win = Tk()
win.title('Tiny Grabber GUI')
win.columnconfigure(0, weight=1)
win.rowconfigure(1, weight=1)
DEBUG = 1

class mainWindow(Frame):
    def __init__(self):
        Frame.__init__(self, win)

        self.bg = 0xFF0000
        
        self.file_container = {}
        self.params = {}
        self.output_name = ""
        self.file_count = 1
        self.bin_name = "tiny_grabber"
        self.bin_path = ''
    
        self.extension = ""
        
        
        
        self.addfile_bt = Button(win, text="Add File..", command=self.cmd_open_file, width=10)
        self.addfile_bt.grid(row=0, column=0)
      
        self.lbFiles = Listbox(win)
        self.lbFiles.grid(row=1, column=0)
        
        self.btRemoveAll = Button(win, text="Remove All Files", command=None, width=12)
        self.btRemoveAll.grid(row=2,column=0)
        
        self.btProcess = Button(win, text="Process", command=self.process_data, width=12)
        self.btProcess.grid(row=1, column=1)
        



        
    def cmd_open_file(self):
    
        filename= askopenfilename(filetypes=(
            [('All', '*.*'),
            ('Image', '*.bmp'),
            ('Image', '*.jpg'),
            ('Image', '*.png'),
            ('Image', '*.pcx'),
            ('Image', '*.gif'),
            ('Sound', '*.wav'),
            ('Sound', '*.aiff'),
            ('Sound', '*.ogg'),
            ('Sound', '*.mp3'),]
            
            
        ))
        
        if not filename:
            showerror('Open File', "Failed to load file {0}".format(filename))
            return
        elif os.path.basename(filename) in self.file_container:
            showwarning('Open File', '{0} is already inserted in list'.format(os.path.basename(filename)))
            return

        self.file_container [os.path.basename(filename)] = (self.file_count, filename)
        container = self.file_container[os.path.basename(filename)]
        self.lbFiles.insert(container[0], os.path.basename(filename))
        self.file_count += 1
           
    def process_data(self):
        
        if self.lbFiles.size() <= 0:
            showerror('Error:', 'Please add files to generate the package')
            return
        
        files = [ s[1][1] for s in self.file_container.items() ]
        files = " ".join(files)
        
        bin  = ""
        self.params = {'-f' : files,
                       '-o' : 'testui.dat'
                       }
        
        args = [self.params]
        
        p = None
        if sys.platform.startswith('linux'):
            binary_file = "tiny_grabber"
            
        elif sys.platform.startswith('win32'):
            if DEBUG == 1:
                binary_file = os.path.join('..\\Debug', 'tiny_grabber.exe')
            else:
                binary_file = "tiny_grabber.exe"
        else:
            binary_file = "tiny_grabber"
        args.insert(0, binary_file)
        
        command_line = [ " ".join(s) for s in args[1].items() ]
        command_line = " ".join(command_line)
        command_line = binary_file + " " + command_line

        try:
            with subprocess.Popen(command_line, stdout=subprocess.PIPE) as proc:
                print(str( proc.communicate(proc.stdout)[0]) )
                if proc.stderr:
                    print("ERROR: " + str(proc.communicate(proc.stderr)) )
        except Exception as m:
            showerror('Error', str(m))
        
        
        
        
        
        
       
       

def run():
    w = mainWindow()
    w.mainloop()
