#include <cstdlib>
#include "tg.h"
#if defined(_WIN32)
#include "libs\getopt_port\getopt.h"
#endif

#if defined(__GNUC__)
#include <iostream>
#include <unistd.h>
#include <getopt.h>
#include <cctype>
#include <vector>
#include <map>
#endif

#include <cstring>

using namespace  TinyGrabber;


#define MAX_PARAMS 50
#define MAX_OUTPUT_FILENAME 255

typedef struct program_file_args {
	unsigned pos;
	char name[56]; 
	int locked;

}program_file_args;

static program_file_args opt_files[MAX_PARAMS];
char output_filename[56];
char input_filename[56];
int create_mode = 0;
int load_mode = 0;
int compressed = 0;
static std::vector<std::string> files_args;

#if _WIN32
#define strdup(f) _strdup(f)
#elif __GNUC__
#define strdup(f) strdup(f)
#endif

#if _WIN32
//windows miss this function :(
static char* strsep(char** stringp, const char* delim)
{
	char* start = *stringp;
	char* p;

	p = (start != NULL) ? strpbrk(start, delim) : NULL;

	if (p == NULL)
	{
		*stringp = NULL;
	}
	else
	{
		*p = '\0';
		*stringp = p + 1;
	}

	return start;
}
#endif


static void  alloc_params(program_file_args **t, const char *name) {
	
	if (strcmp(name, " ") == 0 || strcmp(name, "") == 0 || name == nullptr) return;
	
	program_file_args *tmp = new program_file_args;

	if (tmp == nullptr) {
		tmp = new program_file_args;
	}

	strcpy(tmp->name, name);
	tmp->locked = 1;

	*(t) = tmp;

}


static void tg_tokenize(const char *str) {
	char *tmp = nullptr;
	char *token = nullptr;
	
    tmp = strdup(str);

	int c = 0;
    while ((token = strsep(&tmp, ",")) != nullptr) {
		strcpy(opt_files[c].name, token);
		opt_files[c].locked = 1;
        opt_files[c].pos = static_cast<uint32_t>(c);
		++c;

        files_args.push_back(token);

        printf("DBG: %s\n", files_args.back().c_str());
		//alloc_params(&opt_files[++c], token);
	}
	
	if(tmp != nullptr) delete[] tmp;
	if(token != nullptr) delete[] token;
	
	
}

static struct option opts[MAX_PARAMS] = {
{ "files",required_argument, nullptr, 'f' },
{ "output", optional_argument, nullptr, 'o' },
{ "new", optional_argument, nullptr, 'n' },
{ "load", optional_argument, nullptr, 'l' },
{ "compress", optional_argument, nullptr, 'c' },
{ "extract", optional_argument, nullptr, 'x' },
{ nullptr, 0, nullptr, '\0' }
};


static char *files_buf = nullptr;


static void tg_filename_create() {
	if (files_buf == nullptr) {
		files_buf = new char[MAX_OUTPUT_FILENAME];
	}
	strcpy(files_buf, "");
}


static void usage(void) {

	printf("tinygrabber -f		<files> -o <output_file>\n");
	printf("-f <files>  - add files\n");
	printf("-c		adds file compression\n");
	printf("-o <name>		add files\n");
	printf("-l		load the archive\n");
    printf("-x		extract archive files\n");
	exit(0);
}


int main(int argc, char *argv[]) {
	int opt;
	

	if (argc <= 1) {
		usage();
	}

    while ((opt = getopt_long(argc, argv, "f:ol:ncx:", opts, nullptr)) != -1) {

		switch (opt) {

		case 'c':
			compressed = 1;
			break;

		case 'l':
			tg_filename_create();
			create_mode = 0;
			load_mode = 1;
			optind--;

			for (; optind < argc && *argv[optind] != '-'; optind++) {
				strcat(input_filename, argv[optind]);
			}
			break;

		case 'n':
			break;
		case 'f':
			tg_filename_create();
			create_mode = 1;
			if (create_mode) {
				optind--;
				for (; optind < argc && *argv[optind] != '-'; optind++) {
					strcat(files_buf, argv[optind]);
					strcat(files_buf, " ");
				}
			}

			if (load_mode) {
				strcat(files_buf, argv[optind]);
			}

			//tokenize files parameters
			tg_tokenize(files_buf);



			break;
		case 'o':
			tg_filename_create();
			strcpy(output_filename, argv[optind]);
			break;

		default:
			usage();
			break;
		}

	}

    if (files_buf == nullptr) {
		files_buf = new char[MAX_OUTPUT_FILENAME];
		strcpy(files_buf, "out.dat");

	}

	if (create_mode) {
		tg_create_file(output_filename);

		for (int i = 0; i < MAX_PARAMS; i++) {
			if (opt_files[i].locked == 0) break;
			tg_insert_new_file(opt_files[i].name);
			continue;
		}

		tg_save_to_file();
		tg_close();

	}

	if (load_mode) {
		tg_load_file(input_filename);
		tg_show_metadata();
	}
	


	return EXIT_SUCCESS;
}
