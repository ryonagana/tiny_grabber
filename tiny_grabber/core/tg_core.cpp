#include "../tg.h"
#include <iostream>
#include <cctype>
#include <cstdint>
#include <cstring>
#include "../compression/tg_compression.h"

using namespace  TinyGrabber;

extern tg_header_block header;
extern tg_file_block   files[TG_MAX_FILES];
extern unsigned fileblock_index;
extern char output_filename[56];
extern int compressed;
static FILE *output = nullptr;

static bool tg_check_version(int maj, int min, int build);
static void tg_update_header();

 uint_least64_t  TinyGrabber::tg_getSize(FILE *fp) {
	
	if(fp == nullptr){
		return 0;	
	}	

	long lastPos = 0;
	long size = 0;
	lastPos = ftell(fp);
	rewind(fp);
	fseek(fp, 0L, SEEK_END);
	size = ftell(fp);

	rewind(fp);
	fseek(fp, lastPos, SEEK_CUR);

	return (uint_least64_t)size;

}

void TinyGrabber::tg_create_file(const std::string & filename)
{
	
	output = fopen(filename.c_str(), "wb");
	fwrite(&header, sizeof(header), 1, output);
	printf("=== Creating FILE ===\n\n");
	
}

void TinyGrabber::tg_insert_new_file(const std::string & filename)
{
	tg_file_block block;
	FILE *in = nullptr;
	int i;

	strncpy(block.filename, filename.c_str(), filename.size() + 1);
	block.size = 0;
	block.locked = 1;

	in = fopen(filename.c_str(), "rb");
	block.size = tg_getSize(in);

	if (in == nullptr) {
		printf("-- %s not found.. ignoring\n",filename.c_str());
		return;
	}

	for (i = 0; i < TG_MAX_FILES; ++i) {
        if (files[fileblock_index].locked != 1 || &files[fileblock_index] == nullptr) {
			files[fileblock_index] = block;
			header.files_count = ++fileblock_index;
			fprintf(stdout, "%s Added to archive\n", block.filename);
			break;
		}
	}

	fclose(in);




}

static void tg_update_header(){
	rewind(output);
	fwrite(&header, sizeof(header), 1, output);
	printf("\n\n--Wrote Changes in File Header--\n\n");
}

void TinyGrabber::tg_save_to_file_compressed(){
    int i;

    for(i = 0; i < TG_MAX_FILES; i++){

        if(&files[i] == nullptr || files[i].locked != 1){
            break;
        }

        tg_file_block block = files[i];
        FILE *in = nullptr;
        uint_least64_t size = 0;
        char *buf = nullptr;
        in = fopen(block.filename, "rb");

        if (in == nullptr) continue;

        size = tg_getSize(in);
        buf = new char[size];
        fread(buf, sizeof(unsigned char) * size,1, in);

        uint8 *dst = new uint8[size];
        mz_ulong dst_len = compressBound(size);
        tg_compress(dst, &dst_len, buf,size);
        fwrite(dst, sizeof(uint8) * dst_len,1,output);
        fclose(in);
        delete[] buf;
        buf = nullptr;
        delete[] dst;
        dst = nullptr;
    }
    for (i = 0; i < TG_MAX_FILES; i++) {
        if(&files[i] == nullptr || files[i].locked != 1){
            break;
        }
        printf("-- compressed: %s", files[i].filename);
    }

 tg_update_header();
 return;
}

void TinyGrabber::tg_save_to_file()
{
	int i;
	long size = 0;




	for (i = 0; i < TG_MAX_FILES; i++) {
        if (&files[i] ==  nullptr || files[i].locked != 1) {
			break;
		}

		tg_file_block block = files[i];
		size_t bytesWritten = 0;
		fwrite(&block, sizeof(block), 1, output);
		bytesWritten = block.size;
		header.size += bytesWritten;
	}

    /* write compressed files */


    if(compressed == 1){

        tg_save_to_file_compressed();
        return;
    }


	for (i = 0; i < TG_MAX_FILES; i++) {

		if (&files[i] == NULL || files[i].locked != 1) {
			break;
		}

		tg_file_block block = files[i];
		FILE *in = nullptr;
		uint_least64_t size = 0;
		char *b = nullptr;
		in = fopen(block.filename, "rb");
		
		if (in == nullptr) continue;
		size = tg_getSize(in);
		b = new char[size];

		fread(b, sizeof(unsigned char) * size, 1, in);
		fwrite(b, sizeof(unsigned char) * size, 1, output);
		fprintf(stdout, "--Writing %s.. \n", block.filename);
		fclose(in);
		delete[] b;
		b = nullptr;

	}

	
    tg_update_header();
    return;
}

void TinyGrabber::tg_load_file(const std::string & filename)
{
	tg_header_block *tmp_header = new tg_header_block;
	FILE *in = nullptr;
	int i;
	uint_least64_t size = 0;


	in = fopen(filename.c_str(), "rb");
	size = tg_getSize(in);

	fread(tmp_header, sizeof(header), 1, in);
	header = *tmp_header;

	for (i = 0; i < header.files_count; i++) {
		tg_file_block *block = new tg_file_block;

		fread(block, sizeof(*block), 1, in);
		files[fileblock_index++] = *block;
		//delete[] block;
	}


}

void TinyGrabber::tg_show_metadata() {
	int i;

	printf("=== HEADER ==\n");
	printf("is Valid CBA: %d\n", tg_check_version(header.version_build, header.version_minor, header.version_build));
	printf("Num. Files: %d\n", header.files_count);
    printf("Size: %lu\n", header.size);
    printf("Compressed Size: %lu\n", header.compressed_size);
	printf("VERSION: %d.%d.%d\n", header.version_major, header.version_minor, header.version_build);
	printf("=== EOF HEADER ==\n");


	for (i = 0; i < TG_MAX_FILES; i++) {
		if (files[i].locked != 1) break;
		printf("==== FILES ===\n");
		printf("Name: %s\n", files[i].filename);
        printf("Size: %lu\n", files[i].size);
        printf("Compressed Size: %lu\n", files[i].compressed_size);
		printf("=== EOF ==\n\n");
	}

}

static bool tg_check_version(int maj, int min, int build) {
    if (TG_FILE_MAJOR_VERSION != maj && TG_FILE_MINOR_VERSION != min) {
		return false;
	}

	
	return true;
}

void TinyGrabber::tg_close() {
	if (output != nullptr) {
		fclose(output);
	}
	printf("=== Closing FILE ===\n\n");
}
