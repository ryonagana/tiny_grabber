#pragma once
#include "../libs/miniz/miniz.h"


extern "C" {

	typedef unsigned char uint8;
	typedef unsigned short uint16;
	typedef unsigned int uint;


	int tg_compress(mz_uint8 *dst, mz_ulong *dst_len, void* src, mz_ulong src_len);
	int tg_decompress(mz_uint8 *dst, mz_ulong *dst_len, void* src, mz_ulong src_len);

}