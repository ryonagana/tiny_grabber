#pragma once
#include <iostream>
#include <cstdlib>
#include <cstdint>

#define TG_FILE_MAJOR_VERSION 0
#define TG_FILE_MINOR_VERSION 2
#define TG_FILE_BUILD_VERSION 5

#define TG_MAX_FILES 255

#include "core/tg_core.h"

typedef struct tg_header_block {
    char header[3];
	unsigned int files_count;
	unsigned int compressed;
	unsigned int version_major;
	unsigned int version_minor;
	unsigned int version_build;
	uint_least64_t size;
	uint_least64_t compressed_size;




}tg_header_block;

typedef struct tg_file_block {
	char filename[56];
	uint_least64_t size;
	uint_least64_t compressed_size;
	int locked;
} tg_file_block;







