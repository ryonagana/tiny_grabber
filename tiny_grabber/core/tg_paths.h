#pragma once
namespace TinyGrabber {
    namespace Paths {

        template<typename T>
        T base_name(T const &filepath, T const & delimiter = "/\\");

        template<typename T>
        T remove_extension(T const & filepath);
    }
}

