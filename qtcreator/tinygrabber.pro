TEMPLATE = app
CONFIG += console c++14
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    ../tiny_grabber/tg.cpp \
    ../tiny_grabber/tiny_grabber.cpp \
    ../tiny_grabber/compression/tg_compression.cpp \
    ../tiny_grabber/core/tg_core.cpp \
    ../tiny_grabber/libs/miniz/miniz.c \
    ../tiny_grabber/core/tg_paths.cpp

HEADERS += \
    ../tiny_grabber/tg.h \
    ../tiny_grabber/compression/tg_compression.h \
    ../tiny_grabber/core/tg_core.h \
    ../tiny_grabber/libs/miniz/miniz.h \
    ../tiny_grabber/core/tg_paths.h

DISTFILES += \
    ../tiny_grabber/linux/Thumbs.db
