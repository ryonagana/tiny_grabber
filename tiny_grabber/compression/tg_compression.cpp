#include "tg_compression.h"
#include <cstdio>
#include <iostream>



extern "C" {
	
	int tg_compress(mz_uint8 *dst, mz_ulong *dst_len, void* src, mz_ulong src_len) {
		
		
        if (dst == nullptr) {
			dst = (mz_uint8 *)calloc(*dst_len, sizeof(size_t));
		}

		int cmp = compress(dst, dst_len, (unsigned char *)src, src_len);
		if (cmp != Z_OK)
		{
			printf(" -- Compression Failed!\n\n");
			return 3;
		}

		return 0;
	}


	int tg_decompress(mz_uint8 *dst, mz_ulong *dst_len, void* src, mz_ulong src_len) {
		
		int cmp_status = 0;
		uint total_succeed = 0;

		cmp_status = uncompress(dst, dst_len, (const unsigned char *)src, src_len);
		total_succeed += (cmp_status == Z_OK);

		if (cmp_status != Z_OK) {
			fprintf(stderr, "Decompression Failed, Sorry\n");
			return 1;
		}

		//check uncompressed data is the same when compressed
		if (*dst_len != src_len || (memcmp(src, dst, src_len))) {
			fprintf(stderr, "Decompression Failed");
			return 1;
		}

		return 0;
	}

	
}
