#include "tg_paths.h"

using namespace  TinyGrabber::Paths;

template<typename T>
T TinyGrabber::Paths::base_name(T const &filepath, T const & delimiter){
      return filepath.substr(filepath.find_last_of(delimiter) + 1);
}


template<typename T>
T TinyGrabber::Paths::remove_extension(T const & filepath){
    typename T::size_type const find_dot(filepath.find_last_of("."));
    return find_dot > 0 && find_dot != T::npos ? filepath.substr(0, find_dot) : filepath;
}
