#pragma once
#include <iostream>
#include <cstdlib>
#include <cstdint>
#include <cstdio>

namespace TinyGrabber {

void tg_show_metadata();
uint_least64_t tg_getSize(FILE *fp);
void tg_create_file(const std::string &filename);
void tg_insert_new_file(const std::string &filename);
void tg_save_to_file();
void tg_save_to_file_compressed();
void tg_load_file(const std::string &filename);
void tg_close();

}
